<?php
/**
 * @file
 * Packaging tests.
 */

/**
 * Packaging tests.
 */
class PHPackageTest extends DrupalWebTestCase {
  /**
   * Declares test.
   */
  public static function getInfo() {
    return array(
      'name' => 'Packaging',
      'description' => 'Test repository packaging.',
      'group' => 'Project Hosting',
    );
  }

  /**
   * Sets up the test.
   */
  protected function setUp() {
    parent::setUp('ph_repository_test', 'ph_package');
    // Suppress repository auto-synchronization.
    variable_set('ph_repository_sync_auto', FALSE);
  }

  /**
   * Tests export wrapper function.
   */
  public function testExport() {
    /** @var $repository PHRepository */
    $repository = entity_create('ph_repository', array('type' => 'test'));
    $repository->save();

    // Test exporting.
    $version = ph_version_make(array(
      'core' => 7,
      'major' => 1,
    ));
    $directory = ph_package_export($repository, $version);
    $this->assertTrue(file_exists($directory), 'Repository is exported into the working directory.');
  }

  /**
   * Tests archiving.
   */
  public function testArchiveHandler() {
    // Test archive handler.
    /** @var $repository PHRepository */
    $repository = entity_create('ph_repository', array('type' => 'test'));
    $repository->save();

    $version = ph_version_make(array(
      'core' => 7,
      'major' => 1,
    ));
    $directory = ph_package_export($repository, $version);
    $archive = new PHPackageArchive($directory, array(
      'version' => $version,
      'project' => 'test',
    ));
    $this->assertTrue($archive instanceof PHPackageArchive, 'Package archive handler is created.');

    $archiveDirectory = 'public://archive';
    file_prepare_directory($archiveDirectory, FILE_CREATE_DIRECTORY);
    variable_set('ph_package_archive_directory', $archiveDirectory);
    $extension = 'tar.gz';
    $fileName = $archive->getFileName($extension);
    $this->assertEqual('test-7.x-1.x-dev.tar.gz', $fileName, 'Package archive handler provides archive file name.');
    $filePath = $archive->getFilePath($extension);
    $this->assertEqual($archiveDirectory, drupal_dirname($filePath));

    $file = $archive->createArchive($extension, array(
      'replace' => FILE_EXISTS_REPLACE,
    ));
    $this->assertTrue(is_object($file), 'Archive file is created.');
    /** @var $archiver ArchiverInterface */
    $archiver = archiver_get_archiver(drupal_realpath($file->uri));
    $contents = $archiver->listContents();
    sort($contents);
    $expected = array(
      'test/LICENSE.txt',
      'test/test.info',
      'test/test.module',
    );
    $this->assertEqual($expected, $contents, 'Archive files contains the exported package.');
  }
}

/**
 * Stamp tests.
 */
class PHPackageStampTest extends DrupalWebTestCase {
  /**
   * Declares test.
   */
  public static function getInfo() {
    return array(
      'name' => 'Stamp',
      'description' => 'Test package stamp functionality.',
      'group' => 'Project Hosting',
    );
  }

  /**
   * Sets up the test.
   */
  protected function setUp() {
    parent::setUp('ph_package');
  }

  /**
   * Tests default stamp handler.
   */
  public function testDefault() {
    $info = ph_package_get_stamp_info();
    $this->assertTrue(isset($info['default']), 'Default stamp is found.');

    // Test creating factory.
    $default_factory = ph_package_stamp_factory();
    $this->assertTrue($default_factory instanceof PHPackageDefaultStampFactory, 'Provided stamp factory is default.');
    $factory = ph_package_stamp_factory('default');
    $this->assertIdentical($default_factory, $factory, 'Default factory object is cached.');

    // Test creating stamp.
    $stamp = $factory->createStamp('temporary://test');
    $this->assertTrue($stamp instanceof PHPackageStampInterface, 'Factory creates stamp object.');
  }

  /**
   * Tests stamp header.
   */
  public function testStampHeader() {
    variable_set('ph_package_stamp_header', '[site:name] test header');
    variable_set('site_name', 'Demo');
    $header = ph_package_get_stamp_header();
    $this->assertEqual('Demo test header', $header, 'Stamp header is formatted using tokens.');
  }

  /**
   * Tests stamping directory.
   */
  public function testStamp() {
    $directory = 'temporary://test';
    $file = $directory . '/test.info';
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    file_put_contents($file, '');
    variable_set('ph_package_stamp_header', 'Header');
    ph_package_stamp($directory, array(
      'version' => ph_version_make(array(
        'core' => 7,
        'major' => 1,
        'patch' => 0,
        'increment' => 2,
      )),
      'project' => 'test',
      'project_status_url' => 'http://test.com/',
    ));
    $contents = file_get_contents($file);

    $this->assertContains("\n; Header\n", $contents, 'Header is stamped.');
    $this->assertContains("\nversion = \"7.x-1.0+2-dev\"\n", $contents, 'Version string is stamped.');
    $this->assertContains("\nproject = \"test\"\n", $contents, 'Project name is stamped.');
    $this->assertContains("\nproject status url = \"http://test.com/\"\n", $contents, 'Project status URL is stamped.');
  }

  /**
   * Asserts matching a string in a body of text.
   */
  protected function assertContains($needle, $haystack, $message = '') {
    if (!$message) {
      $message = 'Given string is found in text';
    }
    $this->assertTrue(strstr($haystack, $needle), $message);
  }
}
